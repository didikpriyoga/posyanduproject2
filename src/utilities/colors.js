export default{
    primaryColor: '#22b4a3',
    whiteColor: '#ffffff',
    blackColor: '#000000',
    redColor: '#ff0000',
}