import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';

class ListScreen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      datas: []
    };
  }

  componentDidMount() {
    if (this.props.type === 'PROVINSI') {
      this._getDatas('provinces')
    }
    if (this.props.type === 'KABUPATEN/KOTA') {
      this._getDatas('cities', `province_id=${this.props.province_id}`)
    }
    if (this.props.type === 'KECAMATAN') {
      this._getDatas('districts', `city_id=${this.props.city_id}`)
    }
    if (this.props.type === 'DESA') {
      this._getDatas('villages', `district_id=${this.props.district_id}`)
    }
    if (this.props.type === 'POSYANDU') {
      this._getDatas('posyandus', `village_id=${this.props.village_id}`)
    }

  }

  async _getDatas(type, params = '') {
    try {
      const url = 'https://eposyandu.temanusaha.com/api/service/'
      const response = await fetch(`${url}${type}?${params}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
      const json = await response.json();

      this.setState({ datas: json })
    } catch (error) {
      console.log(error);
    }
  }

  _setDatas(item) {
    this.props._setDatas(item, this.props.type)
    Navigation.pop(this.props.componentId)
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>

        <Text style={styles.header}>
          LIST {this.props.type}
        </Text>

        {this.state.datas.length > 0 && this.state.datas.map((item, index) => {
          return (
            <TouchableOpacity
              onPress={() => this._setDatas(item)}
              style={styles.listItem}>
              <Text>{item.name}</Text>
            </TouchableOpacity>
          )
        })}

      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
  },
  listItem: {
    margin: 16,
    padding: 16,
    borderWidth: 1,
    borderColor: colors.blackColor,
    borderRadius: 4,
    marginBottom: 4
  }
});

export default ListScreen;
