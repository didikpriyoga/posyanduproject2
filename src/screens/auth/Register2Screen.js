import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';

class Register2Screen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      province: '',
      province_id: '',
      city: '',
      city_id: '',
      district: '',
      district_id: '',
      village: '',
      village_id: '',
      posyandu: '',
      posyandu_id: '',
      errorMessage: ''
    };
  }

  async _goToListScreen(type = '') {
    const error = []

    // if (type === 'KABUPATEN/KOTA' && this.state.province_id === '') {
    //   error.push('Provinsi tidak boleh kosong')
    // }

    if (error.length > 0) {
      this.setState({ errorMessage: error[0] })
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: 'ListScreen',
          passProps: {
            type: type,
            province_id: this.state.province_id,
            city_id: this.state.city_id,
            district_id: this.state.district_id,
            village_id: this.state.village_id,
            _setDatas: (item, type) => this._setDatas(item, type)
          }
        },
      });
    }


  };

  _setDatas(item, type) {
    if (type === 'PROVINSI') {
      this.setState({
        province: item.name,
        province_id: item.id,
        city: '',
        city_id: '',
        district: '',
        district_id: '',
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'KABUPATEN/KOTA') {
      this.setState({
        city: item.name,
        city_id: item.id,
        district: '',
        district_id: '',
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'KECAMATAN') {
      this.setState({
        district: item.name,
        district_id: item.id,
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'DESA') {
      this.setState({
        village: item.name,
        village_id: item.id,
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'POSYANDU') {
      this.setState({
        posyandu: item.name,
        posyandu_id: item.id
      })
    }
  }

  _onBack() {
    Navigation.pop(this.props.componentId)
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>

        <Text style={styles.header}>
          DAFTAR
        </Text>

        <Text style={styles.subHeader}>
          Silahkan isi semua form dibawah ini
        </Text>

        {/* <Text style={{marginHorizontal: 16, color: colors.redColor}}>
          {this.state.errorMessage}
        </Text> */}

        <TextInput
          style={styles.input}
          value={this.state.province}
          placeholder="Provinsi"
          onFocus={() => this._goToListScreen('PROVINSI')}
        />

        <TextInput
          style={styles.input}
          value={this.state.city}
          placeholder="Kabupaten/Kota"
          onFocus={() => this._goToListScreen('KABUPATEN/KOTA')}
        />

        <TextInput
          style={styles.input}
          value={this.state.district}
          placeholder="Kecamatan"
          onFocus={() => this._goToListScreen('KECAMATAN')}

        />

        <TextInput
          style={styles.input}
          value={this.state.village}
          placeholder="Desa"
          onFocus={() => this._goToListScreen('DESA')}

        />

        <TextInput
          style={styles.input}
          value={this.state.posyandu}
          onChangeText={(posyandu) => this.setState({ posyandu })}
          placeholder="Posyandu"
          onFocus={() => this._goToListScreen('POSYANDU')}

        />
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity
            onPress={() => this._onBack()}>
            <Text style={styles.backButton}>Kembali</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this._onRegister()}>
            <Text style={styles.registerButton}>Selanjutnya</Text>
          </TouchableOpacity>
        </View>


      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
  },
  subHeader: {
    margin: 16,
    marginTop: 0,
    fontSize: 16,
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 12,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  backButton: {
    color: colors.redColor,
    textAlign: 'left',
    margin: 16
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'right',
    margin: 16
  }
});

export default Register2Screen;
