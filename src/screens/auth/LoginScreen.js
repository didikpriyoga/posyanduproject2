import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';

class LoginScreen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      x: 1,
      y: 2,
      phoneNumber: '',
      phoneNumberResult: '',
      password: ''
    };
  }

  _onLogin() {
    this.setState(
      {
        phoneNumberResult:
          this.state.phoneNumber
      })
  }

  _onRegister() {
    // console.log('daftar');
    Navigation.push(this.props.componentId, {
      component: {
        name: 'RegisterScreen',
      },
    });
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <Image
          source={require('../../assets/asetLogin.png')}
          style={styles.logo}
        />

        <TextInput
          style={styles.input}
          value={this.state.phoneNumber}
          onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
          placeholder="Masukkan nomor HP"
          keyboardType="numeric"
        />

        <TextInput
          style={styles.input}
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder="Masukkan password"
          keyboardType="default"
          autoCapitalize='none'
          secureTextEntry
        />

        <TouchableOpacity
          style={styles.button}
          onPress={() => this._onLogin()}>
          <Text style={styles.textButton}>MASUK</Text>
        </TouchableOpacity>

        <Text style={styles.dontHaveAccount}>
          Tidak punya akun ?
        </Text>

        <TouchableOpacity
          onPress={() => this._onRegister()}>
          <Text style={styles.registerButton}>DAFTAR</Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  logo: {
    width: 160,
    height: 160,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginVertical: 40
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 12,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'center',
    marginVertical: 16
  }
});

export default LoginScreen;
